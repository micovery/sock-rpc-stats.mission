[TOC]
# sock-rpc-stats Mission#

  * This is a sample Arma 3 mission, that implements the client-side (stats.sqf) for the Node.js [sock-rpc-stats](https://bitbucket.org/micovery/sock-rpc-stats) module.
  * The mission uses the sock.dll extension to manage the underlying TCP/IP connection (server-side)
  * The mission also implements a demo player stats system (pstats.sqf) on top of the base stats.sqf library

## Changelog ##
 * 0.0.7
    * Change underlying protocol to use Raw SQF instead of JSON (improve client performance), requires sock-rpc-stats server v0.0.11 or higher
 * 0.0.6
    * Enhancement: Add support for merge, keys, count
  * 0.0.5
    * Defect fix: BOOLEAN values were being mapped to JSON string instead of JSON boolean
    * Enhancement: Add support for pseudo SQF hash data type using ```sock_hash``` function
  * 0.0.4
    * Defect fix: stats_wipe expected result type was not set correctly
    * Defect fix: SQF NaN values not mapped to valid JSON
  * 0.0.3
    * Enhancement: add support for wipe
  * 0.0.2
    * Enhancement: add support for pop, push, shift, unshift
    * Enhancement: add adapter for iniDB API 
  * 0.0.1 - Initial release


## Guide for setting up the sample mission ##
  * Follow [this guide](https://bitbucket.org/micovery/sock-rpc-stats.mission/src/master/arma3) to setup the sample mission.

## SQF Stats API ##

The stats API is located in the ```lib/stats.sqf``` file. This library provides the generic stats functionality for setting and and getting key values. All of the functions can be called both client-side, and server-side.  

  * stats_set(*scope*, *key*, *value*)

    * *scope* (type: string, required) -  Name of the scope to use
    * *key* (type: string, required) - Name of the key to set  
    * *value* (type: any, required) - Value to set  
        .  
    This function sets the given *key*, and *value* within the specified *scope*. On success, returns ```true```. On failure returns ```false```.

```js
    //Example usage
    [(getPlayerUID player), "name", (name player)] call stats_set;
```
.  

  * stats_set(*scope*, *key-value-pair*, ...)  

    * *scope* (type: string, required) -  Name of the scope to use
    * *key-value-pair* (type: Array, required) -  Key-value pair to set  
      .  
    This function sets one or more *key-value* pairs within the specified *scope*.  On success, returns ```true```, On failure returns ```false```.

```js
    //Examples

    //set a single key-value pair
    ["scope1", ["key1", "value1"]] call stats_set;

    //set two key-value pairs
    ["scope1", ["key1", "value1"], ["key2", "value2"]] call stats_set;
```
.  

  * stats_get(*scope*, *key*, *value*)

    * *scope* (type: string, required) -  Name of the scope to use
    * *key* (type: string, required) - Name of the key to get  
    * *value* (type: any, optional) - Default value to use if *key* does not exist, or an error occurs (default is ```nil```)  
        .  
    This function gets the the value of the given *key*, within the specified *scope*.

```js
    //Examples

    //get the value for "key1"
    ["scope", "key1"] call stats_get;

    //get the value for "key1", or use "default1" if not found
    ["scope", "key1", "default1"] call stats_get;
```
.  

  * stats_get(*scope*, *key-default-pair*, ...)  

    * *scope* (type: string, required) -  Name of the scope to use
    * *key-default-pair* (type: Array, optional) -  Array with *key* to get, and default value to use if the *key* is not found.  
      .  
    This function gets multiple (or all) key-value pairs within the specified *scope*.  
      .  
    On success, returns an array containing the key-value pairs (e.g. ```[["key1","value1"],["key2", "value2"],...]```).  On failure it returns ```nil``.  

```js
    //Examples

    //get the values for all keys within "scope1"
    ["scope1"] call stats_get;

    //get the values for "key1", "key2", and "key3" (note no default value given for "key3")
    ["scope1", ["key1", "default1"], [key2, "default2"], ["key3"]] call stats_get;

```
.  

  * stats_flush(*scope*, ...)  

    * *scope* (type: string, required) -  Name of the scope to flush  
      .  
    This function flushes the data for one more *scopes*.  
    When *scope* is flushed, the data is persisted, and removed from memory. This is useful to call once a player has disconnected from the server.  
    .  
    On success, returns the number of scopes that were flushed.  On failure, returns ```nil```.  
```js
    //Examples

    //flush the stats for "scope1"
    ["scope1"] call stats_flush;

    //flush the stats for "scope1", and "scope2"
    ["scope1", "scope2"] call stats_get;

```
.  

  * stats_wipe(*scope*)  

    * *scope* (type: string, required) -  Name of the scope to wipe  
      .  
    This function wipes all the keys within a specific *scope*.  
    .  
    On success, returns ```true```, On failure returns ```false```.  
```js
    //Examples

    //wipe the stats for "scope1"
    ["scope1"] call stats_wipe;

```

## Player Stats Demo ##

This library uses the lower-level ```lib/stats.sqf``` library to implement a player-centric stats system. It hooks onto the ```onPlayerDisconnected``` event, and it saves the following stats:

* primary weapon (including items, and magazine with ammo count)
* secondary weapon (including items, and magazine with ammo count)
* handgun weapon (including items, and magazine with ammo count)
* position (above terrain level)
* stance 
* damage
* fatigue
* goggles
* headgear
* assigned_items (Compass, GPS, Radio, Watch, Map, etc)
* backpack (with magazines and items)
* vest (with magazines and items)
* uniform (with magazines and items)
* Selected weapon

When the same player reconnects to the server, all the saved stats are restored.  

Player stats are saved per side (Civilian, Bluefor, Opfor, Independent).