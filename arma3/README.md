
[TOC]

# Introduction #
This is a tutorial for setting up, and configuring the sample [sock-rpc-stats.pbo](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/mpmissions/sock-rpc-stats.altis.pbo) mission to work with a dedicated Arma 3 server, and the Node.js stats server.


# Prerequisites #
  * Download and install [Node.js](http://nodejs.org/download/)
  * Install Arma 3 dedicated server by following [these instructions](https://community.bistudio.com/wiki/Arma_3_Dedicated_Server) from the BIS Wiki
  


# Stats server setup, and configuration #

1. Open a new terminal (or command prompt in Windows)  
.    
2. Install the ```sock-rpc-stats``` server using the Node Package Manager (npm)  
   
    * From https://www.npmjs.org/  
    .  
      ```npm install -g sock-rpc-stats```  
    .  
    * From the Git repository  
    .  
      ```npm install -g git+https://micovery@bitbucket.org/micovery/sock-rpc-stats.git```  
.    

3. Start the stats server (using file system as storage) on port ```1337```, and ```localhost```  
.  
```sock-rpc-stats --url=file://./stats --host=localhost --port=1337 --repl```    
.  
For the sake of simplicity, the above command uses the local File-System for storage. However, you can use any of the supported storage systems (MySQL, Redis, MongoDB, couchDB, Cassandra). For more details run: ```sock-rpc-stats --help```


#Arma 3 dedicated game server setup, and mission configuration#
##Linux instructions##


1. Open a new terminal, and switch to the installation directory  
.  
```cd ~/steamcmd/arma3```  
.  
2.  Download the [sock.so](https://bitbucket.org/micovery/sock.dll/raw/v0.0.2/Release/sock.so) extension, and put it in next to the game server binary  
.  
```wget https://bitbucket.org/micovery/sock.dll/raw/v0.0.2/Release/sock.so```  
  .  
3.  Make a folder named ```server``` (will be used for configuration files)  
.  
```mkdir server```  
  .  
4. Put the [arma3.cfg](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/server/arma3.cfg) file in the ```server``` folder  
.  
```wget -P ./server https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/server/arma3.cfg```  
  .  
5. Put the [server.cfg](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/server/server.cfg) file in the ```server``` folder   
.  
```wget -P ./server https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/server/server.cfg```  
  .  
6.  Put the [sock-rpc-stats.altis.pbo](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/mpmissions/sock-rpc-stats.altis.pbo) mission in the ```MPMissions``` folder  
.  
```wget -P ./mpmissions https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/mpmissions/sock-rpc-stats.altis.pbo```    
  .  
7.  Start the game server  
.  
```./arma3server -profiles=server -port=2302 -config=server/server.cfg -cfg=server/arma3.cfg  -sock_host=127.0.0.1 -sock_port=1337```


##Windows instructions##

1. Open Windows Explorer (```Windows Key + E```) and navigate to the Arma 3 installation folder (i.e. the folder that has the ```arma3server.exe``` binary)  
.  
2. Download the [sock.dll](https://bitbucket.org/micovery/sock.dll/raw/v0.0.2/Release/sock.dll) extension, and put it in next to the game server binary  
.  
3. Make a folder named ```server``` (will be used for configuration files)  
.  
4. Put the [arma3.cfg](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/server/arma3.cfg) file in the ```server``` folder   
.  
5. Put the [server.cfg](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/server/server.cfg) file in the ```server``` folder  
.  
6. Put the [sock-rpc-stats.altis.pbo](https://bitbucket.org/micovery/sock-rpc-stats.mission/raw/master/arma3/mpmissions/sock-rpc-stats.altis.pbo) mission in the ```MPMissions``` folder  
.  
7. Make a file called ```server.bat```, open it in Notepad, and put this:  
.  
```arma3server.exe -profiles=server -port=2302 -config=server/server.cfg -cfg=server/arma3.cfg  -sock_host=127.0.0.1 -sock_port=1337```  
.  
8. Save the contents of the ```server.bat``` file, close the editor, and double click on the ```server.bat``` file to execute it.   
