//load the logging library
private["_h"];
_h = [] execVM "lib\log.sqf";
waitUntil {scriptDone _h};

//load the socket rpc library
private["_h"];
_h = [] execVM "lib\sock.sqf";
waitUntil {scriptDone _h};

//load the generic stats library
private["_h"];
_h = [] execVM "lib\stats.sqf";
waitUntil {scriptDone _h};

//load the player specific stats library
private["_h"];
_h = [] execVM "lib\pstats.sqf";
waitUntil {scriptDone _h};
